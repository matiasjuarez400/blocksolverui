# Install
1. Install NodeJS
2. Clone repo and go inside it, then run: `npm install`
3. Start the app with `npm start`

# Settings
You can change the port where it runs using environment variable `PORT`.