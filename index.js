const express = require('express');
const open = require('open');
const path = require('path');

const PORT = process.env.PORT || 3090;
const app = express();

app.use('/', express.static(path.join(__dirname, 'public')));

app.listen(PORT, () => {
	open(`http://localhost:${PORT}`);

	console.log(`Running app on port ${PORT}`);
});