const URL = 'http://192.168.0.169:8080/solve';

document.getElementById('img').addEventListener('change', function(event) {
	setTimeout(() => {
		document.querySelector('label').style.display = 'none';
		document.querySelector('.spinner').style.display = 'block';
	}, 300);

	document.querySelector('label').classList.toggle('hide', true);
	document.querySelector('.spinner').classList.toggle('show', true);

	const files = event.target.files;
	const formData = new FormData();

	formData.append('image', files[0]);

	fetch(URL, {
		method: 'POST',
		body: formData
	})
	.then(response => response.json())
	.then(response => {
		if (!!response.imageSolution) {
			const img = document.createElement('img');
			
			img.src = `data:image/png;base64,${response.imageSolution}`;

			document.getElementById('full').appendChild(img);

			return;
		}

		if (!!response.moreImages) {
			response.moreImages.forEach(image => {
				if (!image) {
					return;
				}

				const img = document.createElement('img');

				img.src = `data:image/png;base64,${image}`;

				document.getElementById('result').appendChild(img);
			});
		}
	})
	.catch(err => console.error(err))
	.finally(() => {
		document.querySelector('.spinner').style.display = 'none';
		document.querySelector('#retry').style.display = 'block';
	});
});